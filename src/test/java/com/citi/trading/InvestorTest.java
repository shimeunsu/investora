package com.citi.trading;

import org.mockito.Mockito;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import org.hamcrest.collection.IsMapContaining;
import org.junit.Test;
import org.mockito.stubbing.OngoingStubbing;

import com.citi.trading.OrderPlacer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;


public class InvestorTest{
	// a mock market for investor
	// Market mockMaket = Mockito.mock (Market.class); 
	
	Investor investor = new Investor(100, new MockMarket());
	
	private static class MockMarket implements OrderPlacer{
	
		public  void placeOrder(Trade order, Consumer<Trade> callback) {
			callback.accept(order);
		}
		
	}
	
	@Test
	public void test() {
		
		investor.buy("GOOG", 10, 1);
		
		
		assertThat(investor.getCash(), closeTo(90.00,0.001));
	    assertThat(investor.getPortfolio(), IsMapContaining.hasEntry("GOOG",10));
		
	    investor.sell("GOOG", 10, 1);
	    assertThat(investor.getCash(), closeTo(100.00,0.001));
	    assertThat(investor.getPortfolio(), IsMapContaining.hasEntry("GOOG",0));
	    
	    
	}
		
	
	}
